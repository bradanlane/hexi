# HEXI Badges

HEXI badges (or just **HEXI**) are hexagonal PCBs with _IN_ and _OUT_ connectors which allow them to be connected together.

The connectors provide power (3V3 and GND) as well as I2c (SCL and SDA). A HEXI does not need to use any of these connections
but must pass them from _IN_ to _OUT_ such that connected badges have access to the pins.

The **HEXI** follows the common [standard hexagon dimensions](https://sticker.how/) used by many sticker providers.
To make PCB design easier, the actual long diagonal dimension is 50mm _(which is within the 4.98mm to 5.18mm tolerance)_.
The hexagon is oriented with a top and bottom vertex and the distance between the parallel sides is 44mm.

There are a pair of _IN_ connectors and a pair of _OUT_ connectors.
The _IN_ connectors are female standard 0.1" headers.
The _OUT_ connectors are male standard 0.1" headers.

The _IN_ connectors are located on the left vertical side and teh upper-left angle side (when looking from the front).
The _OUT_ connectors are on opposing sides to the _IN_connectors.

All connectors are right angle and mounted from the back of the PCB.

The connectors are centered at the middle of their respective edge
with 3.0 mm of the pin/socket within the perimeter of the PCB
and 3.0 mm extending outside the edge of the PCB.

**Note:** Since the placement of the connectors must be consistent across all HEXI, sample KiCAD projects are provided.

## KiCAD Files

To aid with designing new HEXI, there are KiCAD files provided.

## Reference Files

There are two reference projects to help design new HEXI badges - one for using through hole connectors and one for using SMD connectors.
There are also a couple examples of HEXIs.
- **hexi_tht** is a bare bones reference implementation using through-hole connectors
- **hexi_smt** is a bare bones reference implementation using surface mount connectors
- **hexi_smt_ivw** is an example implementation using surface mount connectors and the _I Void Warranties_ artwork with the skull eyes backlight with fast-flash LEDs
- **hexi_smt_thief** is an example implementation using surface mount connectors and the _Data Thief_ artwork from DEFCON 30 Hardware Hacking Village talk "Zer02SAO" with the solid red side view leds and one fast-flash LED
- **hexi_smt_rp2040** is an example implementation which includes a full RP2040 design with an ST7789 display, buttons, accelerometer, USB, LiPo, charging, and a 2000mA LDO which is enough to power multiple attached HEXI. _(This reference has not been tested.)_

There are also several footprints and 3D models provided.
_(Place the footprint and 3D models in the appropriate folders for you KiCAD installation.)_

## Power Considerations

The current HEXI standard* accounts for a maximum of approximately 2000mA of 3V3 power across all connected HEXI.
The power trace is 0.75mm or approximately 30mils. Assuming 1 oz copper on the PCB, this supports 2000mA at 3.3V with a 10C temperature rise.
The truth is, the default PCB layout provides two routes for the 3V3 trace which means most configurations will support significantly more than 2000mA.

## Documentation Recommendations

There is a customer footprint for a 'data block'. This is not required but the relevant information should be printed somewhere on each HEXI.

The Maker's name and/or social information _(Mastodon ID, Twitter Handle, GitHub, personal web URL, etc) are optional.
Stating the power requirements is highly recommended to allow users to add together the expected power needs of all the HEXI they connect.
If a HEXI uses I2C, it is recommended to publish the I2C ID(s) used.

\* _The user of the word 'standard' is loose at best, but we all need to start somewhere.
Please open an issue for improvements to the current standard as well as errors in the reference implementations._
